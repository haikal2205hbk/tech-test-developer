<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Station;
use App\Models\Sensor;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        User::create([
            'name' => 'Muhammad Haikal bin Khalid',
            'email' => 'haikal2205hbk@gmail.com',
            'password' => bcrypt('asdfghjkl')
        ]);

        Station::create([
            'name' => 'UiTM Shah Alam',
            'location' => 'Shah Alam, Selangor',
            'status' => 'operational'
        ]);

        Station::create([
            'name' => 'UiTM Kuala Terengganu (Chendering)',
            'location' => 'Kuala Terengganu, Terengganu',
            'status' => 'non-operational'
        ]);

        Station::create([
            'name' => 'UiTM Puncak Alam',
            'location' => 'Puncak Alam, Selangor',
            'status' => 'operational'
        ]);

        Station::create([
            'name' => 'UiTM Puncak Perdana',
            'location' => 'Puncak Perdana, Selangor',
            'status' => 'non-operational'
        ]);

        Station::create([
            'name' => 'UiTM Dungun',
            'location' => 'Dungun, Terengganu',
            'status' => 'operational'
        ]);

        Sensor::create([
            'name' => 'Temperature Sensor 1',
            'type' => 'Temperature',
            'capabilities' => 'Measures temperature in Celcius',
            'status' => 'online',
            'station_id' => 1
        ]);

        Sensor::create([
            'name' => 'Humidity Sensor 1',
            'type' => 'Humidity',
            'capabilities' => 'Measures humidity in percentage',
            'status' => 'offline',
            'station_id' => 2
        ]);

        Sensor::create([
            'name' => 'Pressure Sensor 1',
            'type' => 'Pressure',
            'capabilities' => 'Measures pressure in Pascals',
            'status' => 'online',
            'station_id' => 3
        ]);

        Sensor::create([
            'name' => 'Motion Sensor 1',
            'type' => 'Motion',
            'capabilities' => 'Detects motion',
            'status' => 'malfunctioning',
            'station_id' => 4
        ]);

        Sensor::create([
            'name' => 'Light Sensor 1',
            'type' => 'Light',
            'capabilities' => 'Measures light in Lux',
            'status' => 'offline',
            'station_id' => 5
        ]);

        Sensor::create([
            'name' => 'Sound Sensor 1',
            'type' => 'Sound',
            'capabilities' => 'Measures sound in decibels',
            'status' => 'online',
            'station_id' => 1
        ]);

        Sensor::create([
            'name' => 'CO2  Sensor 1',
            'type' => 'CO2',
            'capabilities' => 'Measures CO2 in ppm',
            'status' => 'online',
            'station_id' => 2
        ]);

        Sensor::create([
            'name' => 'Dust Sensor 1',
            'type' => 'Dust',
            'capabilities' => 'Measures dust in mg/m³',
            'status' => 'online',
            'station_id' => 3
        ]);

        Sensor::create([
            'name' => 'Water Sensor 1',
            'type' => 'water',
            'capabilities' => 'Detects water presence',
            'status' => 'offline',
            'station_id' => 4
        ]);

        Sensor::create([
            'name' => 'Vibration Sensor 1',
            'type' => 'Vibration',
            'capabilities' => 'Measures vibration',
            'status' => 'malfunctioning',
            'station_id' => 5
        ]);
    }
}
