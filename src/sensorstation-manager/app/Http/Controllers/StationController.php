<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Station;
use App\Models\Sensor;

class StationController extends Controller
{
    public function index() {
        return view('station.stationList', [
            "stations" => Station::latest()->get(),
            "title" => "List of Stations",
            "active" => "station",
            "titlePage" => "Station List"
        ]);
    }

    // to go to details station page
    public function show(Station $station) {
        $sensors = Sensor::where('station_id', $station->id)->get();
        return view('station.detailsStation', [
            "title" => "Details Station",
            "active" => "station",
            "titlePage" => "Station Details",
            "station" => $station,
            "sensors" => $sensors
        ]);
    }

    // to go to add new station page
    public function create()
    {
        return view('station.addStation', [
            "title" => "Add New Station",
            "active" => "station",
            "titlePage" => "Add Station",
        ]);
    }

    // to add new station data 
    public function store(Request $request) {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'location' => 'required|max:255',
            'status' => 'required|max:255',
        ]);

        $station = Station::create($validatedData);

        return redirect('/station')->with('success', 'New station has been added');
    }

    // to the edit station page
    public function edit(Station $station) {
        return view('station.editStation', [
            "title" => "Edit Station",
            "active" => "station",
            "titlePage" => "Edit Station",
            "station" => $station
        ]);
    }

    // to edit station data
    public function update(Request $request, Station $station) {
        $rules = [
            'name' => 'required|max:255',
            'location' => 'required|max:255',
            'status' => 'required|max:255',
        ];

        $validatedData = $request->validate($rules);

        Station::where('id', $station->id)->update($validatedData);

        return redirect('/station')->with('success', 'Station data has been updated');
    }

    // to delete a sensor
    public function destroy(Station $station) {
        Station::destroy($station->id);

        return redirect('/station')->with('success', 'Station has been deleted');
    }
}
