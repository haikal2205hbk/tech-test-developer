<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class RegisterController extends Controller
{
    public function index() {
        return view('register.registerPage', [
            "titlePage" => "Registration"
        ]);
    }

    public function store(Request $request) {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email:dns|unique:users',
            'password' => 'required|min:6|max:255',
            'confirmpassword' => 'required|min:6|max:255|same:password'
        ]);

        User::create($validatedData);

        return redirect('/login')->with('success', 'Registration successful. You can login now');
    }
}
