<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sensor;
use App\Models\Station;

class SensorController extends Controller
{
    public function index() {
        return view('sensor.sensorList', [
            "sensors" => Sensor::latest()->get(),
            "title" => "List of Sensors",
            "active" => "sensor",
            "titlePage" => "Sensor List"
        ]);
    }

    // to sensor details page
    public function show(Sensor $sensor) {
        $station = Station::findOrFail($sensor->station_id);
        return view('sensor.detailsSensor', [
            "title" => "Details Sensor",
            "active" => "sensor",
            "titlePage" => "Sensor Details",
            "sensor" => $sensor,
            "station" => $station
        ]);
    }

    // to go to add new sensor page
    public function create()
    {
        $stations = Station::all();
        return view('sensor.addSensor', [
            "title" => "Add New Sensor",
            "active" => "sensor",
            "titlePage" => "Add Sensor",
            "stations" => $stations
        ]);
    }

    // to add new sensor data 
    public function store(Request $request) {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'type' => 'required|max:255',
            'capabilities' => 'required|max:255',
            'status' => 'required|max:255',
            'station_id' => 'required|exists:stations,id'
        ]);

        $sensor = Sensor::create($validatedData);

        return redirect('/sensor')->with('success', 'New sensor has been added');
    }

    // to the edit sensor page
    public function edit(Sensor $sensor) {
        $stations = Station::all();
        return view('sensor.editSensor', [
            "title" => "Edit Sensor",
            "active" => "sensor",
            "titlePage" => "Edit Sensor",
            "sensor" => $sensor,
            "stations" => $stations
        ]);
    }

    // to edit sensor data
    public function update(Request $request, Sensor $sensor) {
        $rules = [
            'name' => 'required|max:255',
            'type' => 'required|max:255',
            'capabilities' => 'required|max:255',
            'status' => 'required|max:255',
            'station_id' => 'required|exists:stations,id'
        ];

        $validatedData = $request->validate($rules);

        Sensor::where('id', $sensor->id)->update($validatedData);

        return redirect('/sensor')->with('success', 'Sensor data has been updated');
    }

    // to delete a sensor
    public function destroy(Sensor $sensor) {
        Sensor::destroy($sensor->id);

        return redirect('/sensor')->with('success', 'Sensor has been deleted');
    }
}
