<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function Sensor() {
        return $this->hasMany(Sensor::class)->onDelete('cascade');
        // return $this->hasMany(Sensor::class)->onDelete(function ($sensor) {
        //     // Define the behavior when a related Sensor model is deleted
        //     $sensor->delete();
        // });
    }
}
