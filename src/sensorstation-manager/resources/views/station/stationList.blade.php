@extends('main')

@section('container')
    <h1>{{ $title }}</h1>

    @if (session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif

    <div class="d-flex my-3">
        <a href="/station/create" class="btn btn-primary me-2">Add new station</a>
    </div>

    <div class="table-responsive small col-lg-12">    
        <table class="table table-bordered table-hover table-sm">
            <thead class="table-dark">
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Location</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                    <th scope="col" style="width: 150px;">Last Update</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($stations as $station) 
                    <tr>
                        <td>{{ $station->name }}</td>
                        <td>{{ $station->location }}</td>
                        <td class="{{ ($station['status'] === 'operational' ? 'bg-success' : 'bg-danger') }}">{{ $station['status'] }}</td>
                        <td>
                            <div class="d-flex">
                                <a href="/station/{{ $station->id }}" class="btn btn-primary btn-sm mx-1" title="details"><i class="bi bi-eye"></i></a>
                                <a href="/station/{{ $station->id }}/edit" class="btn btn-warning btn-sm mx-1" title="edit"><i class="bi bi-pencil-square"></i></a>
                                <form action="/station/{{ $station->id }}" method="post">
                                    @method('delete')
                                    @csrf
                                    <button class="btn btn-danger btn-sm mx-1" onclick="return confirm('Are you sure?')" title="delete"><i class="bi bi-trash"></i></button>
                                </form>
                            </div>
                        </td>
                        <td>{{ $station->updated_at->diffForHumans() }}</td>                  
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection