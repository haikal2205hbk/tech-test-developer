@extends('main')

@section('container')
<h1>{{ $title }}</h1>

<div class="d-flex my-3">
    <a href="/station" class="btn btn-secondary me-2"><i class="bi bi-caret-left-fill"></i> back</a>
    <a href="/station/{{ $station->id }}/edit" class="btn btn-warning me-2" title="edit"><i class="bi bi-pencil-square"></i> Edit</a>
    <form action="/station/{{ $station->id }}" method="post">
        @method('delete')
        @csrf
        <button class="btn btn-danger" onclick="return confirm('Are you sure?')" title="delete"><i class="bi bi-trash"></i> Delete</button>
    </form>
</div>

<form>
    @csrf
    <div class="row my-2">
        <div class="col-md-6">
            <div class="form-group mb-2">
                <label for="name">Station Name</label>
                <input type="text" class="form-control" id="name" value="{{ $station->name }}" disabled>
            </div>
            <div class="form-group my-2">
                <label for="location">Location</label>
                <input type="text" class="form-control" id="location" value="{{ $station->location }}" disabled>
            </div>
            <div class="form-group my-2">
                <label for="status">Status</label>
                <input type="text" class="form-control" id="status" value="{{ $station->status }}" disabled>
            </div>
        </div>
    </div>
</form>

<h1>Sensors Available</h1>

<div class="table-responsive small col-lg-12">    
    <table class="table table-bordered table-hover table-sm">
        <thead class="table-dark">
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Type</th>
                <th scope="col">Capabilities</th>
                <th scope="col">Location Station</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
                <th scope="col" style="width: 150px;">Last Update</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($sensors as $sensor) 
                <tr>
                    <td>{{ $sensor->name }}</td>
                    <td>{{ $sensor->type }}</td>
                    <td>{{ $sensor->capabilities }}</td>
                    <td>{{ $sensor->station->name }}</td>
                    <td class="{{ ($sensor['status'] === 'online') ? 'bg-success' : (($sensor['status'] === 'offline') ? 'bg-warning' : 'bg-danger') }}">{{ $sensor['status'] }}</td>
                    <td>
                        <div class="d-flex">
                            <a href="/sensor/{{ $sensor->id }}" class="btn btn-primary btn-sm mx-1" title="details"><i class="bi bi-eye"></i></a>
                            <a href="/sensor/{{ $sensor->id }}/edit" class="btn btn-warning btn-sm mx-1" title="edit"><i class="bi bi-pencil-square"></i></a>
                            <form action="/sensor/{{ $sensor->id }}" method="post">
                                @method('delete')
                                @csrf
                                <button class="btn btn-danger btn-sm mx-1" onclick="return confirm('Are you sure?')" title="delete"><i class="bi bi-trash"></i></button>
                            </form>
                        </div>
                    </td>
                    <td>{{ $sensor->updated_at->diffForHumans() }}</td>                  
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection