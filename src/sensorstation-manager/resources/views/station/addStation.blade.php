@extends('main')

@section('container')
    <h1>{{ $title }}</h1>

    <a href="/station" class="btn btn-secondary my-3"><i class="bi bi-caret-left-fill"></i> back</a>

    <form action="/station" method="post">
        @csrf
        <div class="row my-2">
            <div class="col-md-6">
                <div class="form-group mb-2">
                    <label for="name">Station Name</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') }}" required>
                    @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group my-2">
                    <label for="location">Location</label>
                    <input type="text" class="form-control @error('location') is-invalid @enderror" id="location" name="location" value="{{ old('location') }}" required>
                    @error('location')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="status" class="form-label">Status</label>
                    <select class="form-select @error('status') is-invalid @enderror" id="status" name="status" required>
                        <option value="" selected disabled></option>
                        <option value="operational" {{ (old('status') === 'operational') ? 'selected' : '' }}>operational</option>
                        <option value="non-operational" {{ (old('status') === 'non-operational') ? 'selected' : '' }}>non-operational</option>
                    </select>
                    @error('status')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="d-flex">
            <button type="submit" class="btn btn-primary my-3 me-2">Submit</button>
            <a href="/sensor" class="btn btn-secondary my-3">Cancel</a>
        </div>
    </form>
@endsection