@extends('main')

@section('container')
    <h1>{{ $title }}</h1>

    @if (session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif

    <div class="d-flex my-3">
        <a href="/sensor/create" class="btn btn-primary me-2">Add new sensor</a>
    </div>

    <div class="table-responsive small col-lg-12">    
        <table class="table table-bordered table-hover table-sm">
            <thead class="table-dark">
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Type</th>
                    <th scope="col">Capabilities</th>
                    <th scope="col">Location Station</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                    <th scope="col" style="width: 150px;">Last Update</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($sensors as $sensor) 
                    <tr>
                        <td>{{ $sensor->name }}</td>
                        <td>{{ $sensor->type }}</td>
                        <td>{{ $sensor->capabilities }}</td>
                        <td>{{ $sensor->station->name }}</td>
                        <td class="{{ ($sensor['status'] === 'online') ? 'bg-success' : (($sensor['status'] === 'offline') ? 'bg-warning' : 'bg-danger') }}">{{ $sensor['status'] }}</td>
                        <td>
                            <div class="d-flex">
                                <a href="/sensor/{{ $sensor->id }}" class="btn btn-primary btn-sm mx-1" title="details"><i class="bi bi-eye"></i></a>
                                <a href="/sensor/{{ $sensor->id }}/edit" class="btn btn-warning btn-sm mx-1" title="edit"><i class="bi bi-pencil-square"></i></a>
                                <form action="/sensor/{{ $sensor->id }}" method="post">
                                    @method('delete')
                                    @csrf
                                    <button class="btn btn-danger btn-sm mx-1" onclick="return confirm('Are you sure?')" title="delete"><i class="bi bi-trash"></i></button>
                                </form>
                            </div>
                        </td>
                        <td>{{ $sensor->updated_at->diffForHumans() }}</td>                  
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection