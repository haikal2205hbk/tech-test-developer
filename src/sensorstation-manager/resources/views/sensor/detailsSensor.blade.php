@extends('main')

@section('container')
    <h1>{{ $title }}</h1>

    <div class="d-flex my-3">
        <a href="/sensor" class="btn btn-secondary me-2"><i class="bi bi-caret-left-fill"></i> back</a>
        <a href="/sensor/{{ $sensor->id }}/edit" class="btn btn-warning me-2" title="edit"><i class="bi bi-pencil-square"></i> Edit</a>
        <form action="/sensor/{{ $sensor->id }}" method="post">
            @method('delete')
            @csrf
            <button class="btn btn-danger" onclick="return confirm('Are you sure?')" title="delete"><i class="bi bi-trash"></i> Delete</button>
        </form>
    </div>

    <form>
        @csrf
        <div class="row my-2">
            <div class="col-md-6">
                <div class="form-group mb-2">
                    <label for="name">Sensor Name</label>
                    <input type="text" class="form-control" id="name" value="{{ $sensor->name }}" disabled>
                </div>
                <div class="form-group my-2">
                    <label for="type">Type (e.g., motion, light, sound)</label>
                    <input type="text" class="form-control" id="type" value="{{ $sensor->type }}" disabled>
                </div>
                <div class="form-group my-2">
                    <label for="capabilities">Capabilities</label>
                    <input type="text" class="form-control" id="capabilities" value="{{ $sensor->capabilities }}" disabled>
                </div>
                <div class="form-group my-2">
                    <label for="status">Status</label>
                    <input type="text" class="form-control" id="status" value="{{ $sensor->status }}" disabled>
                </div>
                <div class="form-group">
                    <label for="station_id" class="form-label">Station Location</label>
                    <input type="text" class="form-control" id="station_id" value="{{ $station->name }}" disabled>
                </div>
            </div>
        </div>
    </form>
@endsection