@extends('main')

@section('container')
    <h1>{{ $title }}</h1>

    <a href="/sensor" class="btn btn-secondary my-3"><i class="bi bi-caret-left-fill"></i> back</a>

    <form action="/sensor" method="post">
        @csrf
        <div class="row my-2">
            <div class="col-md-6">
                <div class="form-group mb-2">
                    <label for="name">Sensor Name</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') }}" required>
                    @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group my-2">
                    <label for="type">Type (e.g., motion, light, sound)</label>
                    <input type="text" class="form-control @error('type') is-invalid @enderror" id="type" name="type" value="{{ old('type') }}" required>
                    @error('type')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group my-2">
                    <label for="capabilities">Capabilities</label>
                    <input type="text" class="form-control @error('capabilities') is-invalid @enderror" id="capabilities" name="capabilities" value="{{ old('capabilities') }}" required>
                    @error('capabilities')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="status" class="form-label">Status</label>
                    <select class="form-select @error('status') is-invalid @enderror" id="status" name="status" required>
                        <option value="" selected disabled></option>
                        <option value="online" {{ (old('status') === 'online') ? 'selected' : '' }}>online</option>
                        <option value="offline" {{ (old('status') === 'offline') ? 'selected' : '' }}>offline</option>
                        <option value="malfunctioning" {{ (old('status') === 'malfunctioning') ? 'selected' : '' }}>malfunctioning</option>
                    </select>
                    @error('status')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="station_id" class="form-label">Station Location</label>
                    <select class="form-select @error('station_id') is-invalid @enderror" id="station_id" name="station_id" required>
                        <option value="" selected disabled></option>
                        @foreach($stations as $station)
                            <option value="{{ $station->id }}" {{ (old('station_id') == $station->id) ? 'selected' : '' }}>{{ $station->name }}</option>
                        @endforeach
                    </select>
                    @error('station_id')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="d-flex">
            <button type="submit" class="btn btn-primary my-3 me-2">Submit</button>
            <a href="/sensor" class="btn btn-secondary my-3">Cancel</a>
        </div>
    </form>
@endsection