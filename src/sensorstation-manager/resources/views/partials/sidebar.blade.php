<div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
    {{-- title in sidebar and x button to close the sidebar --}}
    <div class="offcanvas-header">
        <h5 class="offcanvas-title" id="offcanvasExampleLabel">SensorStation Manager</h5>
        <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    {{-- all pages directory --}}
    <div class="offcanvas-body">
        <div class="list-group">
            {{-- to dahsboard page --}}
            <a href="/" class="list-group-item list-group-item-action {{ ($active === "dashboard") ? 'active' : '' }}">
                <i class="bi bi-speedometer2"></i> Dashboard
            </a>
            {{-- to station management page --}}
            <a href="/station" class="list-group-item list-group-item-action {{ ($active === "station") ? 'active' : '' }}">
                <i class="bi bi-geo-alt"></i> Station Management
            </a>
            {{-- to sensor management page --}}
            <a href="/sensor" class="list-group-item list-group-item-action {{ ($active === "sensor") ? 'active' : '' }}">
                <i class="bi bi-thermometer"></i> Sensor Management
            </a>

        </div>
    </div>
</div>