<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\SensorController;
use App\Http\Controllers\StationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// home / dashboard page
Route::get('/', function () {
    return view('dashboard.dashboardHome', [
        "title" => "Dashboard",
        "active" => "dashboard",
        "titlePage" => "Home"
    ]);
})->middleware('auth');

// for sensor
Route::resource('/sensor', SensorController::class)->middleware('auth');
// Routes to show the details of a sensor
// Route::get('/sensor/{$sensor:id }', [SensorController::class, 'show'])->middleware('auth');

// for station
Route::resource('/station', StationController::class)->middleware('auth');

// For login, logout and register user
Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::post('/logout', [LoginController::class, 'logout']);


Route::get('/register', [RegisterController::class, 'index'])->middleware('guest');
Route::post('/register', [RegisterController::class, 'store']);